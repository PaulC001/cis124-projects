/*
    Name: Shane Goldsberry
    Date: 09/04/18
*/

import java.util.Scanner;

public class OutputWithVars {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        // Variables used
        int userNum;
        int userNumSq;
        int userNumCbd;
        int userNum2;
        int numSum;
        int numProd;
        int userNum3;
        int numProdAll;
        int numSubAll;

        // Get and display first integer
        System.out.println("Enter integer:");
        userNum = scnr.nextInt();
        System.out.println("You entered: " + userNum);

        // Square and cube first integer
        userNumSq = (userNum * userNum);
        userNumCbd = (userNum * userNum * userNum);
        System.out.println(userNum + " squared is " + userNumSq);
        System.out.println("And " + userNum + " cubed is " + userNumCbd + "!!");

        // Get second integer
        System.out.println("Enter another integer:");
        userNum2 = scnr.nextInt();

        // Calculate and display sum and product of the two numbers
        numSum = (userNum + userNum2);
        numProd = (userNum * userNum2);
        System.out.println(userNum + " + " + userNum2 + " is " + numSum);
        System.out.println(userNum + " * " + userNum2 + " is " + numProd);

        // Get third integer and display all three
        System.out.println("Enter one more integer:");
        userNum3 = scnr.nextInt();
        System.out.println("You have now entered" + userNum + ", " + userNum2 + " and " + userNum3);

        // Perform final calculations and disply results
        numProdAll = numProd * userNum3;
        numSubAll = userNum - userNum2 - userNum3;
        System.out.println(userNum + " * " + userNum2 + " * " + userNum3 + " = " + numProdAll);
        System.out.println(userNum + " - " + userNum2 + " - " + userNum3 + " = " + numSubAll);
    }
}